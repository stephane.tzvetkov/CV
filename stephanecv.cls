\ProvidesClass{stephanecv}[25/09/2017 CV class]
\LoadClass{article}
\NeedsTeXFormat{LaTeX2e}

%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
%	 PACKAGES NECESSAIRES ET CONFIGURATIONS ASSOCIEES
%

\RequirePackage[quiet]{fontspec}
\RequirePackage[sfdefault]{ClearSans}
\RequirePackage{tikz}
\RequirePackage{xcolor}
\RequirePackage[absolute,overlay]{textpos}
\RequirePackage{ragged2e}
\RequirePackage{etoolbox}
\RequirePackage{ifmtarg}
\RequirePackage{ifthen}
\RequirePackage{pgffor}
\RequirePackage{marvosym}
\RequirePackage{parskip}

\usepackage{multicol}

\usepackage{smartdiagram}

\usetikzlibrary{arrows}

\usepackage{enumitem}
\setlist[itemize]{leftmargin=*}

\RequirePackage[hidelinks]{hyperref}
\hypersetup{
    pdftitle={Stéphane TZVETKOV Curriculum Vitae},
    pdfauthor={Stéphane TZVETKOV},
    pdfsubject={},
    pdfkeywords={},
    colorlinks=false,
    allbordercolors=white
}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax

\ifxetex
  \usepackage{letltxmacro}
  \setlength{\XeTeXLinkMargin}{1pt}
  \LetLtxMacro\SavedIncludeGraphics\includegraphics
  \def\includegraphics#1#{
    \IncludeGraphicsAux{#1}
  }
  \newcommand*{\IncludeGraphicsAux}[2]{
    \XeTeXLinkBox{
      \SavedIncludeGraphics#1{#2}
    }
  }
\fi

%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
%	 CONFIGURATIONS COULEURS
%

\definecolor{white}{RGB}{255,255,255}
\definecolor{gray}{HTML}{4D4D4D}
\definecolor{sidecolor}{HTML}{E7E7E7}
\definecolor{mainblue}{HTML}{0E5484}
\definecolor{maingray}{HTML}{B9B9B9}

\definecolor{pblue}{HTML}{0395DE}

\definecolor{darkgray}{HTML}{333333}
\definecolor{gray}{HTML}{4D4D4D}
\definecolor{lightgray}{HTML}{999999}
\definecolor{green}{HTML}{C2E15F}
\definecolor{orange}{HTML}{FDA333}
\definecolor{purple}{HTML}{D3A4F9}
\definecolor{red}{HTML}{FB4485}
\definecolor{blue}{HTML}{6CE0F1}
\definecolor{kleinblue}{HTML}{002FA7}
\definecolor{seriousblue}{HTML}{0E5384}
\definecolor{materialpurple}{HTML}{9C27B0}
\definecolor{materialindigo}{HTML}{3F51B5}
\definecolor{materialblue}{HTML}{2196F3}
\definecolor{materialcyan}{HTML}{00BCD4}
\definecolor{materialteal}{HTML}{009688}
\definecolor{materialgreen}{HTML}{4CAF50}
\definecolor{materiallime}{HTML}{CDDC39}
\definecolor{materialamber}{HTML}{FFC107}
\definecolor{materialbrown}{HTML}{795548}
\definecolor{materialred}{HTML}{FF4436}
\definecolor{materialorange}{HTML}{FF5722}

\ifdefined\@cv@print
  \colorlet{green}{gray}
  \colorlet{orange}{gray}
  \colorlet{purple}{gray}
  \colorlet{red}{gray}
  \colorlet{blue}{gray}
  \colorlet{fillheader}{white}
  \colorlet{header}{gray}
\else
  \colorlet{fillheader}{white}
  \colorlet{header}{gray}
\fi
\colorlet{textcolor}{gray}
\colorlet{headercolor}{gray}

%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
%	 DIVERS CONFIGURATIONS
%

\pagestyle{empty} 			% Désactive les headers et les footers

\setlength{\parindent}{0pt} % Désactive l indentation des paragraphes

%-------------------------------------------------------------------------------
% ------------------------------------------------------------------------------
% 	POLICE
%

\newfontfamily\headingfont[Path = fonts/]{segoeuib.ttf} % Police "Segoe"

%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
% DEFINITION DE LA COMMANDE GENERANT LE DIAGRAMME CIRCULAIRE
%
% cf. https://tex.stackexchange.com/questions/82727
%			  create-a-ring-diagram-in-tex/82729#82729

% Adjusts the size of the wheel:
\def\innerradius{1.0cm}
\def\outerradius{2.0cm}

% Macro principale
\newcommand{\wheelchart}[1]{
	\begingroup\centering
    % Calculate total
    \pgfmathsetmacro{\totalnum}{0}
    \foreach \value/\colour/\name in {#1} {
        \pgfmathparse{\value+\totalnum}
        \global\let\totalnum=\pgfmathresult
    }

    \begin{tikzpicture}

      % Calculate the thickness and the middle line of the wheel
      \pgfmathsetmacro{\wheelwidth}{\outerradius-\innerradius}
      \pgfmathsetmacro{\midradius}{(\outerradius+\innerradius)/2}

      % Rotate so we start from the top
      \begin{scope}[rotate=90]

      % Loop through each value set.
	  % \cumnum keeps track of where we are in the wheel
      \pgfmathsetmacro{\cumnum}{0}
      \foreach \value/\colour/\name in {#1} {
            \pgfmathsetmacro{\newcumnum}{\cumnum + \value/\totalnum*360}

            % Calculate the percent value
            \pgfmathsetmacro{\percentage}{\value/\totalnum*100}
            % Calculate the mid angle of the colour segments to place the labels
            \pgfmathsetmacro{\midangle}{-(\cumnum+\newcumnum)/2}

            % This is necessary for the labels to align nicely
            \pgfmathparse{
               (-\midangle<180?"west":"east")
            } \edef\textanchor{\pgfmathresult}
            \pgfmathsetmacro\labelshiftdir{1-2*(-\midangle>180)}

            % Draw the color segments. Somehow, the \midrow units got lost,
            % so i add 'pt' at the end. Not nice...
            \fill[\colour] (-\cumnum:\outerradius) arc (-\cumnum:-(\newcumnum):\outerradius) --
            (-\newcumnum:\innerradius) arc (-\newcumnum:-(\cumnum):\innerradius) -- cycle;

            % Draw the data labels
            \draw  [*-,thin] node [append after command={(\midangle:\midradius pt) -- (\midangle:\outerradius + 1ex) -- (\tikzlastnode)}] at (\midangle:\outerradius + 1ex) [xshift=\labelshiftdir*0.5cm,inner sep=0pt, outer sep=0pt, ,anchor=\textanchor]{\name};

            % Set the old cumulated angle to the new value
            \global\let\cumnum=\newcumnum
        }

      \end{scope}
%      \draw[gray] (0,0) circle (\outerradius) circle (\innerradius);
    \end{tikzpicture}
	\endgroup
}

%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
%	 DEFINITION DES COMMANDES DE LA BARRE LATERALE
%

\setlength{\TPHorizModule}{1cm}	% Left margin
\setlength{\TPVertModule}{1cm}	% Top margin

\newlength\imagewidth
\newlength\imagescale
\pgfmathsetlength{\imagewidth}{5cm}
\pgfmathsetlength{\imagescale}{\imagewidth/600}

\newcommand{\profilesection}[2]{\vspace{8pt}{\color{black!80} \huge #1 \rule[0.15\baselineskip]{#2}{0.7mm}}}

% Définition des commandes personnelles liées à la barre latérale
% (pour les infos perso)
\newcommand{\cvdate}[1]{\renewcommand{\cvdate}{#1}}
\newcommand{\cvlinkedin}[1]{\renewcommand{\cvlinkedin}{#1}}
\newcommand{\cvmail}[1]{\renewcommand{\cvmail}{#1}}
\newcommand{\cvnumberphone}[1]{\renewcommand{\cvnumberphone}{#1}}
\newcommand{\cvadresse}[1]{\renewcommand{\cvadresse}{#1}}
\newcommand{\cvinfo}[1]{\renewcommand{\cvinfo}{#1}}
\newcommand{\cvsite}[1]{\renewcommand{\cvsite}{#1}}
\newcommand{\aboutme}[1]{\renewcommand{\aboutme}{#1}}
\newcommand{\profilepic}[1]{\renewcommand{\profilepic}{#1}}
\newcommand{\cvhead}[1]{\renewcommand{\cvhead}{#1}}
\newcommand{\cvjobtitle}[1]{\renewcommand{\cvjobtitle}{#1}}
\newcommand{\cvphoto}[1]{\renewcommand{\cvphoto}{#1}}

% Définition de la commande pour afficher les icônes associés aux infos perso
\newcommand*\icon[1]{\tikz[baseline=(char.base)]{\node[shape=circle,draw,inner sep=1pt, fill=seriousblue,seriousblue,text=white] (char) {#1};}}

% Définition de la commande affichant les tags
\newcommand{\cvtag}[1]{%
  \tikz[baseline]\node[anchor=base,draw=darkgray!30,rounded corners,inner xsep=1ex,inner ysep =0.75ex,text height=1.5ex,text depth=.25ex]{#1};
}

% Définition de la commande pour afficher le digramme des compétences en bulles
%\smartdiagramset{
%  /tikz/bubble center node/.append style={draw=seriousblue,thick,fill=none},
%  /tikz/bubble node/.append style={draw=seriousblue,thick,fill=none},
%}

\newcommand\skills{ 
~
	\smartdiagram[bubble diagram]{
        %\textbf{Software}\\\textbf{Development},
        \textbf{Computing}\\\textbf{Development},
        \textbf{Object}\\\textbf{Oriented}\\\textbf{Prog},
        \textbf{~~~~~Git~~~~~},
        \textbf{Build \& Test}\\\textbf{Automation},
        \textbf{~~~~~UML~~~~~},
        \textbf{User}\\\textbf{Interface},
        \textbf{Unit \&}\\\textbf{Integration}\\\textbf{Testing},
        \textbf{Design}\\\textbf{Pattern}
    }
}

% Définition de la commande pour afficher les barres de progression des intérêts
\newcommand\interests[1]{ 
	\renewcommand{\interests}{
		\begin{tikzpicture}
			\foreach [count=\i] \x/\y in {#1}{
				\draw[fill=maingray,maingray] (0,\i) rectangle (6,\i+0.4);
				\draw[fill=white,mainblue](0,\i) rectangle (\y,\i+0.4);
				\node [above right] at (0,\i+0.4) {\x};
			}
		\end{tikzpicture}
	}
}

%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
%  DEFINITION DE LA COMMANDE DISPOSANT LA BARRE LATERALE
%
% appels des commandes et configs précédentes pour mettre en page
% et afficher la barre latérale lorsque la commande "\makeprofile" sera utilisée

\newcommand{\makeprofile}{

  % Insertion des bandeaux gris
  \begin{tikzpicture}[remember picture,overlay]

	  % bandeau latéral droit :
      \node [rectangle, fill=sidecolor, anchor=north, minimum width=9cm, minimum
	  		 height=\paperheight+1cm] (box) at (-5cm,0.5cm){}; 

  \end{tikzpicture}
  
  \begin{tikzpicture}[remember picture,overlay]

	  % bandeau latéral droit
      \node [rectangle, fill=maingray, anchor=north,
	  		 minimum width=\paperwidth+8cm, minimum height=1.6cm] (box) at
			(-1cm,+1.5cm){}; 

  \end{tikzpicture}

  \begin{textblock}{6}(0.8, 0.2)
	
    % <Prénom, Nom, Titre de l emploi actuel, Photo>
    %------------------------------------------------
    \cvhead{
    
      %\profilesection{\mbox{Ingénieur développement logiciel, Air
	  %						 Traffic Management}}{-1.0cm}{\vspace{-0.0cm}}
      \vspace{-0.2cm}
      %\huge{\color{black!80} \mbox{\textbf{Ingénieur développement logiciel,
	  %									   Air Traffic Management}}}
	  \huge{\color{black!80} \hspace{3.0cm} \mbox{\textbf{Ingénieur développement informatique}}}
      \vspace{-1.2cm}
      
      \begin{multicols}{2}
      
        \LARGE{Stéphane \\ Tzvetkov}
  
        %\huge{Stéphane Tzvetkov}
        %\vspace{0.1cm}
        %\small{\\Ingénieur \\ \mbox{développement logiciel},
		%\\ \mbox{Air Traffic Management}\\}
    
        %\hfill \includegraphics[height=2.666cm]{./img/stephane_circular_id.jpg}
        \includegraphics[height=2.666cm]{./img/stephane_circular_id.jpg}
    
      \end{multicols}
    }
    %------------------------------------------------
	% </Prénom, Nom, Titre de l emploi actuel, Photo>
    
    \vspace{-1.5cm}
    
    % <Infos perso>
    %------------------------------------------------
    \renewcommand{\arraystretch}{1.6}
    \begin{tabular}{p{1cm} @{\hskip 0.3cm}p{4.5cm}}

	  \ifthenelse{\equal{\cvadresse}{}}{}{
            {$
              \begin{array}{l}
              \includegraphics[scale=0.10]{img/adresse.png}
              \end{array}
              $} 
            & \cvadresse\\}
            
      \ifthenelse{\equal{\cvinfo}{}}{}{
            {$
              \begin{array}{l}
              \includegraphics[scale=0.020]{img/info.png}
              \end{array}
              $} 
            & \cvinfo\\}
            
      \ifthenelse{\equal{\cvnumberphone}{}}{}{
            {$
              \begin{array}{l}
              \includegraphics[scale=0.066]{img/tel.png}
              \end{array}
              $} 
            & \cvnumberphone\\}
            
      \ifthenelse{\equal{\href{\cvsite}{\cvsite}}{}}{}{
            {$
              \begin{array}{l}
              {\href{\cvsite}{\includegraphics[scale=0.033]{img/globe.png}}}
              \end{array}
              $}
            & \cvsite\\}
            
      \ifthenelse{\equal{\cvmail}{}}{}{
            {$
              \begin{array}{l}
              {\href{mailto:\cvmail}{\includegraphics[scale=0.033]{img/email.png}}}
              \end{array}
              $}
             & \cvmail\\}
            %& \href{mailto:\cvmail}{\cvmail}}
    \end{tabular}
    %------------------------------------------------
    % </Infos perso>
    
    \vspace{-0.2cm}
    
    % <Compétences>
    %------------------------------------------------
    \profilesection{Compétences}{1.5cm}{\vspace{0.25cm}}
   	\skills
    %------------------------------------------------
    % </Compétences>
        
    \vspace{-0.2cm}
    
    % <Intérêts>
    %------------------------------------------------
    \profilesection{Intérêts}{3.33cm}{\vspace{0.25cm}}
                
    \interests{	{Artificial intelligence/4.8},
        		{Software architecture/5},
        		{Free and open source softwares/5.15},
                {Data privacy and cybersecurity/5.3},
                {GNU\textbackslash Linux (Gentoo)/5.5}}
    \interests
    %------------------------------------------------
    % </Intérêts>
    
    \vspace{0.2cm}
    
    % <Technologies>
    %------------------------------------------------
    \profilesection{Technologies}{1.7cm}{}
    
    \cvtag{Git} \cvtag{SVN} \cvtag{(neo)Vim} \\
    \cvtag{Eclipse SE, RCP} \cvtag{Sirius} \cvtag{EMF-GMF}\\
    \cvtag{JavaDoc} \cvtag{Maven} \cvtag{JUnit} \\
    \cvtag{SonarLint} \cvtag{Docker} \cvtag{SSH} \\
    \cvtag{CodeCoverage} \cvtag{UML Designer} \\
    \cvtag{Doxygen} \cvtag{CMake} \\
    \cvtag{MySQL} \cvtag{ObjectDB} \cvtag{Cassandra} \\
    \cvtag{Neo4J} \cvtag{MongoDB} \cvtag{PostgreSQL} \\
    \cvtag{VirtualBox} \cvtag{VMware} \cvtag{Vagrant} \\
    
    \vspace{-0.25cm}
    %------------------------------------------------
    % </Technologies>
    
  \end{textblock}
}

%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
%	 DEFINITION DES SECTIONS ET SOUS-SECTIONS ET COLORATION DE LEUR TITRE
%

% Définition de la commande colorant les trois premières lettres des titre de
% chaque section
\newcommand*\round[2]{
	\tikz[baseline=(char.base)]\node[anchor=north west, draw,rectangle, rounded
									 corners, inner sep=1.6pt, minimum
									 size=5.5mm, text height=3.6mm,
									 fill=#2,#2,text=white](char){#1};
}

\def\@sectioncolor#1#2#3{
	{%
		\color{seriousblue} #1#2#3%
	}%
}

% Définition de la commande créant une section
\renewcommand{\section}[1]{
  %\par\vspace{\parskip}
	{
		\LARGE\headingfont\color{headercolor}
		\@sectioncolor #1
	}
  \par\vspace{.125\parskip}
}

% Définition de la commande créant une sous-section
\renewcommand{\subsection}[1]{
	\par\vspace{.5\parskip}{
		\Large\headingfont\color{headercolor} #1
	}
	\par\vspace{.25\parskip}
}

\pagestyle{empty}

%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
%	 DEFINITION DE MON ENVIRONNEMENT PERSO POUR LES "LONGUES" LISTES
%

\setlength{\tabcolsep}{0pt}

% Nouvel environnement pour les "longues" listes
\newenvironment{stephanecvEnvList}{
	\begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}ll}
}{
	\end{tabular*}
}

\newcommand{\myitem}[5]{
	#1&\parbox[t]{0.83\textwidth}{
		\textbf{#2}
		\hfill
		{\footnotesize#3}\\
        \ifblank{#4}{}{#4 \vspace{0.2cm}}
		#5\vspace{\parsep}
	}\\
}

%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
%	 DEFINITION DE MON ENVIRONNEMENT PERSO POUR LES "PETITES" LISTES
%

\setlength{\tabcolsep}{0pt}

% Nouvel environnement pour les "petites" listes
\newenvironment{stephanecvShortEnvList}{
	\begin{tabular*}{\textwidth}{@{\extracolsep{\fill}}ll}
}{
	\end{tabular*}
}

\newcommand{\twentyitemshort}[2]{
	#1&\parbox[t]{0.83\textwidth}{
		#2
	}\\
}

%-------------------------------------------------------------------------------
%-------------------------------------------------------------------------------
%	 MARGES ET CONFIGURATION DU DIAGRAMME A BULLES
%

\RequirePackage[left=7.6cm,top=0.1cm,right=1cm,bottom=0.2cm,nohead,nofoot]{geometry}

\usepackage{smartdiagram}
\smartdiagramset{
    bubble center node font = \footnotesize,
    bubble node font = \footnotesize,
    % specifies the minimum size of the bubble center node
    bubble center node size = 0.5cm,
    %  specifies the minimum size of the bubbles
    bubble node size = 0.5cm,
    % specifies which is the distance among the bubble center node and the other
	% bubbles
    distance center/other bubbles = 0.3cm,
    % sets the distance from the text to the border of the bubble center node
    distance text center bubble = 0.5cm,
    % set center bubble color
    bubble center node color = seriousblue,
    % define the list of colors usable in the diagram
    set color list = {lightgray, lightgray, lightgray, lightgray, lightgray,
	lightgray, lightgray, lightgray, lightgray, lightgray},
    % sets the opacity at which the bubbles are shown
    bubble fill opacity = 0.4,
    % sets the opacity at which the bubble text is shown
    bubble text opacity = 1,
    /tikz/bubble node/.append style={draw=none,thick},
}

\tikzset{
    bubble center node/.append style={text=white},
	%bubble node/.append style={text=white}
}

